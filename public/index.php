<?php

require dirname(__DIR__) . '/vendor/autoload.php';

// Load .env variables
$dotenv = Dotenv\Dotenv::createImmutable(dirname(__DIR__));
$dotenv->load();

// load config file
require dirname(__DIR__) . '/config/config.php';

ini_set('display_errors', 1);

error_reporting(E_ALL);

require('routes.php');

