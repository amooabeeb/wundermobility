<?php

use Bootstrap\Router;
use Bootstrap\Request;

use App\Controllers\UserController;
use Bootstrap\Session;

Session::start();

$userController = new UserController();
$router = new Router(new Request);

$router->get('/', function ($request) use ($userController) {
    $userController->getStepOneView($request);
});

$router->post('/', function ($request) use ($userController) {
    return $userController->addPersonalInformation($request);
});

$router->get('/stepTwo', function ($request) use ($userController) {
    $userController->getStepTwoView($request);
});

$router->post('/stepTwo', function ($request) use ($userController) {
    return $userController->addContactInformation($request);
});

$router->get('/stepThree', function ($request) use ($userController) {
    $userController->getStepThreeView($request);
});

$router->post('/stepThree', function ($request) use ($userController) {
    return $userController->addPaymentInformation($request);
});

$router->get('/success', function ($request) use ($userController) {
    $userController->getSuccessView($request);
});
