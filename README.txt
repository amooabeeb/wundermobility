# Basic MultiStep Registration App
The sample app for Wunder Mobility Backend Developer Role

# Get started
- Ensure docker is installed
- `cd` into the project directory
- Create a `.env` using the format from `.env.example`
- Run `docker-compose up -d` to startup the application
- Run `docker-compose exec database mysql -u [username] -p [database_name] < schema.sql` to migrate SQL Schema
- The application can now be accessed on `localhost:8050`

# Possible Performance Optimizations
- The MYSQL DB can be indexed for faster retrieval
- The application can be divided into a Client - Server architecture and the information saved on the client, such that
at the last stage, all the information is collated and sent just once (this will reduce the number of DB calls to just 2)


# Things that can be done better
- Routing can be done better to cater for 404 pages
- Dependency Injection can be introduced
- Request Validation on the backend


