<?php

namespace App\Factories;

use App\Models\User;

class UserFactory
{

    public static function createUserInfo($data)
    {
        $user = new User();
        return $user->create($data);
    }

    public static function updateContactAddress($user_id, $data)
    {
        $user = new User();
        return $user->update($user_id, $data);
    }

    public static function updatePaymentInfo($user_id, $data)
    {
        $user = new User();
        return $user->update($user_id, $data);
    }
}
