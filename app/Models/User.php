<?php

namespace App\Models;

use Bootstrap\Database;

class User extends Model
{

    private $table_name = 'users';

    public function __construct()
    {

    }

    public function create($data)
    {
        $sql = "INSERT INTO $this->table_name (first_name, last_name, telephone, created_at) VALUES (:first_name, :last_name, :telephone, :created_at)";
        $db = Database::getDB();
        $stmt = $db->prepare($sql);
        $stmt->execute([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'telephone' => $data['telephone'],
            'created_at' => date('Y-m-d H:i:s'),
        ]);
        return $db->lastInsertId();
    }

    public function update($user_id, $data)
    {
        $sql = "UPDATE $this->table_name SET ";
        foreach ($data as $key => $value) {
            $sql = $sql." $key = :$key,";
        }
        $sql = substr($sql, 0, -1);
        $sql = $sql." WHERE id = :id";
        $db = Database::getDB()->prepare($sql);
        $data['id'] = $user_id;
        return $db->execute($data);
    }

}
