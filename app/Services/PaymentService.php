<?php

namespace App\Services;

use GuzzleHttp\Client;

class PaymentService
{
    const URL = 'https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data';

    public static function postData($data)
    {
        $client = new Client();
        $response = $client->request('POST', PaymentService::URL, [
            'json' => $data
        ]);
        $response->getStatusCode();
        return $response->getBody();
    }
}
