<?php


namespace App\Controllers;

use App\Factories\UserFactory;
use App\Services\PaymentService;
use Bootstrap\Redirect;
use Bootstrap\RequestInterface;
use Bootstrap\Session;
use Bootstrap\View;

class UserController
{

    public function getStepOneView(RequestInterface $request)
    {
        if (Session::exists('userId') && Session::exists('step')) {
            $step = Session::get('step');
            $this->redirectUserToAppropriateStep((int)$step);
        } else {
            View::renderTemplate('registration/stepone.html');
        }
    }

    private function redirectUserToAppropriateStep(int $step)
    {
        switch($step) {
            case 1:
                Redirect::to('/stepTwo');
                break;
            case 2:
                Redirect::to('/stepThree');
                break;
            default:
                Redirect::to('/');
                break;
        }
    }

    public function addPersonalInformation(RequestInterface $request)
    {
        $request_data = $request->getBody();
        $id = UserFactory::createUserInfo($request_data);
        Session::put('userId', $id);
        Session::put('step', 1);
        Redirect::to('/stepTwo');
    }

    public function getStepTwoView(RequestInterface $request)
    {
        View::renderTemplate('registration/steptwo.html');
    }

    public function addContactInformation(RequestInterface $request)
    {
        $request_data = $request->getBody();
        unset($request_data['submit']);
        $user_id = Session::get('userId');
        UserFactory::updateContactAddress($user_id, $request_data);
        Session::put('step', 2);
        Redirect::to('/stepThree');
    }

    public function getStepThreeView(RequestInterface $request)
    {
        View::renderTemplate('registration/stepthree.html');
    }

    public function addPaymentInformation(RequestInterface $request)
    {
        $request_data = $request->getBody();
        unset($request_data['submit']);
        $user_id = Session::get('userId');
        UserFactory::updatePaymentInfo($user_id, $request_data);
        $payment_request_data = [
            'owner' => $request_data['account_owner'],
            'iban' => $request_data['iban'],
            'customerId' => $user_id
        ];
        $payment_response_data = PaymentService::postData($payment_request_data);
        $payment_response_data = json_decode($payment_response_data, true);
        UserFactory::updatePaymentInfo($user_id, ['payment_data_id' => $payment_response_data['paymentDataId']]);
        Session::put('payment_data_id', $payment_response_data['paymentDataId']);
        Redirect::to('/success');
    }

    public function getSuccessView(RequestInterface $request)
    {
        if (Session::exists('payment_data_id')) {
            $payment_data_id = Session::get('payment_data_id');
            Session::destroy();
            View::renderTemplate('registration/success.html', ['payment_data_id' => $payment_data_id]);
        } else {
            Redirect::to('/');
        }

    }

}


