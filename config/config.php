<?php

define('DB_HOST', getenv('DATABASE_HOST'));
define('DB_NAME', getenv('DATABASE_NAME'));
define('DB_USERNAME', getenv('DATABASE_USERNAME'));
define('DB_PASSWORD', getenv('DATABASE_PASSWORD'));
