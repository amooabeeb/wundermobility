SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `wunder_mobility`
--
CREATE DATABASE IF NOT EXISTS `wunder_mobility_db` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `wunder_mobility`;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `telephone` varchar(255) NOT NULL,
  `street` varchar(255) NULL,
  `house_number` int(11) NULL,
  `zip_code` varchar(255) NULL,
  `city` varchar(255) NULL,
  `account_owner` varchar(255) NULL,
  `iban` varchar(255) NULL,
  `payment_data_id` varchar(512) NULL,
  `created_at` varchar(255) DEFAULT NULL,
  `updated_at` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
