<?php

namespace Bootstrap;

interface RequestInterface
{
    public function getBody();
}
