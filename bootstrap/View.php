<?php

namespace Bootstrap;

class View
{

    public static function render($view, $args = [])
    {
        extract($args, EXTR_SKIP);

        $file = dirname(__DIR__) . "/views/$view";

        if (is_readable($file)) {
            require $file;
        } else {
            throw new \Exception("$file not found");
        }
    }

    /** Render views with twig  **/

    public static function renderTemplate($template, $args = [])
    {
        static $twig = null;

        if ($twig === null) {
            $loader = new \Twig_Loader_Filesystem(dirname(__DIR__) . '/views');
            $twig = new \Twig_Environment($loader);
            $twig->addFunction(new \Twig_SimpleFunction('asset', function ($asset) {
                return sprintf('../%s', ltrim($asset, '/'));
            }));
        }

        echo $twig->render($template, $args);
    }
}
