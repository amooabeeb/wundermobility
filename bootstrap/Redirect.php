<?php

namespace Bootstrap;


class Redirect
{

    public static function to($redirect_uri)
    {
        return (header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL)));
    }
}
