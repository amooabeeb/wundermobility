<?php

namespace Bootstrap;

use Exception;
use PDO;
use PDOException;

class Database
{
    private static $db = null;

    public static function getDB()
    {
        try {
            if (self::$db === null) {
                $dsn = 'mysql:host=' .DB_HOST . ';dbname=' . DB_NAME;
                self::$db = new PDO($dsn, DB_USERNAME, DB_PASSWORD);
                self::$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            }

        } catch (PDOException $e) {
            echo 'DB Connection failed: ', $e->getMessage();
        }
        return self::$db;
    }
}
